import './App.css';
import Nav from './components/Nav';
import Home from './components/Home';
import Form from './components/Form';
import About from './components/About';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">

        <Nav />

        <Switch>

          <Route exact path="/Form">
            <Form />
          </Route>

          <Route exact path="/About">
            <About />
          </Route>

          <Route path="/">
            <Home />
          </Route>

        </Switch>

      </div>
    </Router>
  );
}

export default App;
