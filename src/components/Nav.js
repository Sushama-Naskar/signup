import {Link} from 'react-router-dom';
import './nav.css';

function Nav() {
    return (
        <div className='nav'>
        <Link to="/" className='item'>Home</Link>
         <Link to="/Form" className='item'>Sign Up</Link>
        <Link to="/About" className='item'>About</Link>
        </div>
    )}

    export default Nav;