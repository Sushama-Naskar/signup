import React from 'react';
import './form.css';
import validator from 'validator';

class Form extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            inputValue: {
                name: null,
                email: null,
                age: null,
                password: null,
                confirmPassword: null,
                checkbox: false

            },
            errorValue: {
                nameError: null,
                emailError: null,
                ageError: null,
                passwordError: null,
                confirmPasswordError: null,
                checkboxError: null
            },
            formStatus: {
                status: 'notsubmitted',
                message: null
            }
        }
    }

    handleChange = (e) => {
        const { inputValue, errorValue } = { ...this.state };

        errorValue[e.target.id + 'Error'] = null;

        if (e.target.type === "checkbox") {
            inputValue[e.target.id] = !inputValue.checkbox;
        }
        else {
            inputValue[e.target.id] = e.target.value;
        }

        this.setState({ inputValue, errorValue });
    }

    nameValidate = (value) => {
        if (value === null) {
            return "Enter a valid name";
        }

        if (value.trim().length === 0) {
            return "Enter a valid name";
        } else if (value.length < 2 || value.length > 50) {
            return "Name should have between 2 to 50 characters";
        }

        return 0;
    }

    emailValidate = (value) => {
        if (value === null) {
            return "Enter a valid email";
        }
        if (validator.isEmail(value) === false) {
            return "Enter a valid email";
        }
        return 0;
    }

    ageValidate = (value) => {
        if (value === null) {
            return 'Enter a valid age';
        }

        if (value <= 0 || value > 130) {
            return 'Age should be between 1 to 130';
        }

        return 0;
    }

    passwordValidate = (value) => {
        if (value === null) {
            return "Password is missing";
        }

        if (value.includes(' ')) {
            return "Password should not contain space"
        } else if (value.length < 8 || value.length > 12) {
            return "Password length should be between 8 to 11 characters"
        }

        return 0;
    }

    confirmPasswordValidate = (password, value) => {
        if (value === null) {
            return 'Confirm your Password';
        }

        if (password != value) {
            return 'Password does not match';
        }
        return 0;
    }

    checkboxValidate = (value) => {

        if (value === false) {
            return "Agree to the terms and conditions";
        }
        return 0;

    }

    validateForm = (value) => {
        const { inputValue, errorValue } = { ...this.state };
        let emessage = 0;

        switch (value) {

            case "name": let name = this.nameValidate(inputValue['name']);
                if (name !== 0) {
                    emessage = 1;
                    errorValue.nameError = name;
                }
                break;

            case "email": let email = this.emailValidate(inputValue['email']);
                if (email !== 0) {
                    emessage = 1;
                    errorValue.emailError = email;
                }
                break;

            case "age":
                let age = this.ageValidate(inputValue['age']);
                if (age !== 0) {
                    emessage = 1;
                    errorValue.ageError = age;
                }
                break;

            case "password": let password = this.passwordValidate(inputValue['password']);
                if (password !== 0) {
                    emessage = 1;
                    errorValue.passwordError = password;
                }
                break;

            case "confirmPassword": let confirmPassword = this.confirmPasswordValidate(inputValue['password'], inputValue['confirmPassword']);
                if (confirmPassword !== 0) {
                    emessage = 1;
                    errorValue.confirmPasswordError = confirmPassword;
                }
                break;

            case "checkbox": let checkbox = this.checkboxValidate(inputValue['checkbox']);
                if (checkbox !== 0) {
                    emessage = 1;
                    errorValue.checkboxError = checkbox;
                }
                break;

            default: break;
        }

        this.setState({ errorValue });
        return emessage;
    }

    handleSubmit = (e) => {
        e.preventDefault();

        const { formStatus } = { ...this.state };

        let checkValid = Object.keys(this.state.inputValue).map((element) => {
            return this.validateForm(element);
        })

        if (checkValid.includes(1)) {
            formStatus.status = 'notsubmitted';
            formStatus.message = null;
        } else {
            formStatus.status = 'submitted';
            formStatus.message = 'Form submitted successfully';
        }

        this.setState({ formStatus });
    }

    render() {
        return (
            <div className='main-container'>

                <div className="container">
                    <header className="App-header">Sign Up</header>
                    {this.state.formStatus.status === 'notsubmitted' ?
                        <div>

                            <form onSubmit={this.handleSubmit} className="form">
                                <div>
                                    <label htmlFor="name">Name</label>
                                    <input type="text" id="name" onChange={this.handleChange} /><br />
                                    <div className='errormessage'>{this.state.errorValue.nameError}</div>
                                </div>
                                <div>
                                    <label htmlFor="email">Email</label>
                                    <input type="text" id="email" onChange={this.handleChange} /><br />
                                    <div className='errormessage'>{this.state.errorValue.emailError}</div>
                                </div>
                                <div>
                                    <label htmlFor="age">Age</label>
                                    <input type="number" id="age" onChange={this.handleChange} /><br />
                                    <div className='errormessage'>{this.state.errorValue.ageError}</div>
                                </div>
                                <div>
                                    <label htmlFor="password">Password</label>
                                    <input type="password" id="password" onChange={this.handleChange} /><br />
                                    <div className='errormessage'>{this.state.errorValue.passwordError}</div>
                                </div>
                                <div>
                                    <label htmlFor="confirmPassword">Confirm Password</label>
                                    <input type="password" id="confirmPassword" onChange={this.handleChange} /><br />
                                    <div className='errormessage'>{this.state.errorValue.confirmPasswordError}</div>
                                </div>
                                <div>
                                    <div className="checkbox">
                                        <input type="checkbox" id="checkbox" onClick={this.handleChange} />
                                        <span>Terms and Conditions</span>
                                        <div className='errormessage'>{this.state.errorValue.checkboxError}</div>
                                    </div>

                                </div>
                                <button >Submit</button>
                            </form>
                        </div>
                        : <div className="submitted" >
                            <h1 className="successmessage">{this.state.formStatus.message}</h1>
                            <ul>
                                <li>Name: {this.state.inputValue.name}</li>
                                <li>Email: {this.state.inputValue.email}</li>
                                <li>Age:{this.state.inputValue.age}</li>
                            </ul>
                        </div>}
                </div>
            </div>

        )
    }

}

export default Form;
